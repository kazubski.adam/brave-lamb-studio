# Brave Lamb Studio
## Użyte Paczki
### Paczki zewnętrzne
Do zadania nie użyłem żadnych paczek zewnętrznych. Wykorzystałem grafiki które otrzymałem wraz z opisem zadania.
#### Paczki wewnętrzne
- Universal RP
## Opis projektu
### Utworzenie projektu - 6h
- UI `GameScreen`, `StartScreen`, `ValueView`
- Prosty state machine `StateMachine`, `State`
- Zarządzanie punktacją `Score`
- Ustawienia gry `GameSettings`
- Pool `Pool<T>`
- Rozgrywka `Player`, `Enemy`, `Bullet`, `EnemySpawner`, `BulletSpawner`
- Pool kul `Pool<T>`
- Zarządzanie kolizjami `InteractionHandler2D`, `InteractionReceiver2D`, `IInteractionReceiver2D`
- Liczenie czasu
### "Infinite Scroller"
Świat miał przesuwać się ze stałą prędkością w nieskończoność.
##### Sposób
1. Gracz przesuwa się wyłącznie góra/dół
2. Wrogowie przesuwają się wyłącznie w lewo
3. Pociski przesuwają się wyłącznie w prawo
4. Tło będzie wrzucone na materiał i zmieniany będzie offset
##### Czego nie wziąłem pod uwagę
Teoretycznie powinienem dodać Modulo Node przy ustawianiu offsety UV na osi x z Time Node w ShaderGraphie.
### State Machine
Nim zacząłem projekt pomyślałem: "Zacznę od entry pointu a później będę do niego dołączał pozostałą logikę". Projekt miał mieć dwa możliwe stany:
1. Ekran z punktacją
2. Rozgrywka

Można bawić się w `if` i `else` lub `switch` w `Update()` ale można to zrobić lepiej dlatego stworzyłem prosty `StateMachine` który pozwala na oddzielenie logiki obu stanów przy jednoczesnej możliwości przechodzenia pomiędzy stanami poprzez wykorzystanie abstrakcji i dziedziczenia.
### Poolowanie
Logika wrogów jest prosta i różnic nie ma zbyt wiele (wygląd i startowa wysokość) dlatego postanowiłem wykorzystać pooling obiektów nie tylko dla pocisków ale też i dla wrogów.

Klasa `UnityEngine.Pool.ObjectPool<T>` ([link](https://docs.unity3d.com/ScriptReference/Pool.ObjectPool_1.html)) zawiera publiczną metodę która pozwoliłaby na zwrócenie obiektu przy pomocy indeksu.

Okazało się, że metody tej nie można wywołać (nie jestem pewien czy to bug czy nie) dlatego po kilkunastu minutach researchu w tej sprawie, stworzyłem klasę `Pool<TItem>` która pozwolała na pooling obu kolekcji oraz zwrot wszystkich aktywnych obiektów na żądanie.

##### Uwagi
Klasa `Pool<TItem>` zarządza kolekcją `List<TItem>`. Gdyby znać maksymalną ilość obiektów w takiej kolekcji (np. tutaj maksymalną ilość wrogów aktywnych jednocześnie) można by użyć kolekcję `TItem[]` o ustalonym rozmiarze.
##### Alternatywy
Alternatywą która **mogłaby** być nieco lepsza wydajnościowo byłoby dodanie do klas `Enemy` oraz `Bullet` musiałaby wyglądać m. in.:
``` csharp 
public class ClassName : MonoBehaviour
{
    public static bool shouldReturnToPool;
    public static ObjectPool<ClassName> pool;
    
    private void Update () {
        pool.Release(this);
    }
}
```
Tutaj nie widziałem potrzeby by robić coś takiego.
### Fizyka - Setup
Projekt jest prosty. Postanowiłem skorzystać z warstw i komponentów:
- `Rigidbody2D` z `bodyType = RigidbodyType2D.Kinematic`
- `BoxCollider2D` z `isTrigger = true`

Prędkość ruchu wrogów oraz kul była ustawiana przy pobieraniu ich z poola.

Ruch gracza polegał na podążaniu za wirtualnym pointerem - gracz sterował niewidzialnym punktem za którym samolot podążał. Pozwoliło to swobodę przy określaniu jak "płynnie" ma poruszać się samolot.
##### Uwagi
Moim zdaniem metoda `AddForce(Vector2 forceToAdd)` nie miałby sensu w tym przypadku. Każdy *wróg* czy *kula* mają poruszać się ze stałą prędkością, natomiast *gracz* ma poruszać się góra/dół i nie znaleźć się poza ekranem. Wolę do tego nie dopuścić dlatego kod najpierw weryfikuje nową wirtualną pozycję typu *Vector2* a później dopasowuje `_rigidbody.velocity` tak by samolot podążał za tym wirtualnym punktem.
##### Alternatywa
```csharp 
var time = Time.deltaTime * followUpMultiplier;
var nextPosition = Vector3.Lerp(currentPosition, targetPosition, time);
_rigidbody.MovePosition(nextPosition);
```
### Fizyka - Zarządzanie interakcjami
Pomiędzy 1 a 2 godziną przy projekcie zrozumiałem, że w interakcję mogą wejść ze sobą 2 obiekty lub więcej. Rozważyłem 2 przypadki:
1. Dwa obiekty wchodzą ze sobą w interakcję (np. pocisk oraz samolot). Po rejestracji kolizji oba obiekty znikają - logiczne.
2. Jeden obiekt wchodzi w interakcję z dwoma obiektami (np. diament oraz 2 sloty na diament). Po rejestracji kolizji, diament powinien "wypełnić" jeden ze slotów ale zignorować drugi.

W obu przypadkach można wykorzystać metody jak `OnCollisionEnter` czy `OnCollisionEnter2D` by wykonać określone instrukcje w zależności od sytuacji. Można np. filtrować tagi i/lub użyć `GetComponent<T>()` na drugim obiekcie.

Pomyślałem, że można by uprościć i ustandaryzować ten proces dlatego zatrzymałem timer i zająłem się tym tematem. Ostatecznie stworzyłem klasę `InteractionHandler2D ` która przedstawia łatwą implementację rozwiązania problemu.

Po wznowieniu timera szybciutko zaimplementowałem rozwiązanie a później (już w ramach pracy przy projekcie) nieco rozwinąłem koncept o `InteractionReceiver2D` oraz `IInteractionReceiver2D`.
### Aspekty wizualne
Scorll tła został osiągnięty przez `Material` na którym zmieniany jest offset mapy UV.

Jeżeli chodzi o samoloty - mógłbym wykorzystać `Animator` lub dwa ale nie jestem zwolennikiem tego typu rozwiązań gdy animacje są bardzo proste. Stworzyłem `Shader Graph` dla samolotów i materiał referencyjny.

Raczej nie jestem zwolennikiem tworzenia "milionów" różnych assetów takich jak `Material` w inspektorze. Wolę generować to co potrzebne w trakcie gry.

Jak to działa:
1. Pobieramy materiał referencyjny,
2. Tworzymy kilka materiałów klonów na podstawie materiału referencyjnego
3. Przydzielamy losowy materiał przy pobieraniu nowego *wroga*

Mimo tego, że w każdym możliwym stanie (lot prosto, lot w lewo, lot w prawo) samoloty posiadały tylko dwie klatki animacji, to i tak chciałem urozmaicić projekt więc dla każdego możliwego wyglądu wroga stworzyłem dwa materiały (dla każdego materiału animacja zaczynała się od innej klatki).
### Akcja - Reakcja
W projekcie jest kilka takich wartości których zmiana musi powodować jakieś działania. Pomyślałem o tym, że mogę użyć delegatów do wysyłania callbacków.

Stworzyłem klasę `ValueHandler<TType>` która na to pozwala jednocześnie oddzielając logikę zajumjącą się dotyczącą callbacków i samej wartości.
Wykorzystałem kilka zmiennych typu `System.Action<T>` do odświeżania wartości w UI ale też do sprawdzania czy gracz przegrał lub wygrał czy do wykonywania logiki w klasie `InteractionHandler2D`.
### Losowość
Zaprojektowałem kod tak by maksymalna ilość wrogów jaka może się spawnować zależała od zmiennej w `GameSettings`. Na podstawie tej zmiennej losowana była liczba wrogów która ma się zespawnować naraz oraz to jakie pozycje startowe mogą przyjąć wrogowie.
##### Ilość wrogów na raz
Docelowo spawnować miało się od 2 - 5 wrogów na raz. Zacząłem od najprostszego rozwiązania czyli `Random.Range()` co moim zdaniem sprawiło, że wrogów pojawiało się zbyt wiele.

By uzyskać większą kontrolę nad "losowością" stworzyłem dwie klasy:
```csharp
[Serializable]  
public class RangedArray<TType>  
{
    [SerializeField] private RangedArrayItem<TType>[] _items;  
    
    public int Size => _items.Length;  
	
    public TType Random  
    {  
        get {  
            var sum = 0;  
	
            for (var i = 0; i < _items.Length; i++) sum += _items[i].Range;  
	
            var randomNumber = UnityEngine.Random.Range(0, sum);  
            var toCheck = 0;  
        
            for (var i = 0; i < _items.Length; i++) {  
                toCheck += _items[i].Range;
            
                if (randomNumber < toCheck)	return _items[i].Value;  
            }  
	        
            return _items[0].Value;  
        } 
    }

    public TType Last => _items[^1].Value;  
}

[Serializable]  
public class RangedArrayItem<TType>  
{  
    [field: SerializeField] public TType Value { get; private set; }  
    [field: SerializeField, Range(0, 100)] public int Range { get; private set; }  
}
```
Pobieranie losowej możliwości ze wszystkich dostępnych polegało na:
1. Zsumowaniu `Range` wszystkich elementów tablicy `_items`
2. Wylosowaniu losowej liczby w zakresie od 0 do sumy wyliczonej w 1 punkcie,
3. Sprawdzeniu do której możliwości zalicza się wylosowana liczba
##### Pozycje startowe
Ilość możliwych do przyjęcia pozycji startowych zależała od zmiennej w `GameSettings`.

Te zapisywałem w tablicy a później tasowałem (algorytm Fisher–Yates shuffle) i zwracałem. Każdy spawnowany wróg pobierał kolejny indeks z tablicy i ustawiał się w odpowiedniej pozycji.
### Punktacja
Ekran startowy miał przedstawiać ostatnią uzyskana w trakcie sesji punktacje (o ile takowa istniała). Pomyślałem, że statyczna zmienna idealnie się do tego nada. Poza tą punktacją musiałem mieć gdzieś najlepszy wynik oraz aktualny wynik (w trakcie rozgrywki).

Proste rozwiązania są najlepsze. Nie jestem zwolenników wielkich statycznych klas z masą publicznych wartości (chyba, że to stałe) ale ma to swoje zastosowanie w niektórych przypadkach. Podobnie było w przypadku punktacji.
## Uwagi ogólne
### Cache'owanie
W logice można znaleźć nie optymalne rekalkulacje różnych wartości które docelowo mogły by być cache'owane. Chodzi m. in. o:
1. Krawędzie ekranu (do weryfikacji nowej pozycji gracza, do określenia czy wróg lub kula znalazły się poza ekranem),
2. Time.deltaTime (spora liczbie calli np. w update może kraść nieco mocy obliczeniowej),
3. Suma prawdopodobieństw wszystkich `RangedArray<TType> ` w `RangedArray<TType>`,
4. Pozycje startowe wrogów

Wszystkie te rzeczy można by cache'ować ale tutaj testowanie gry w Play Mode poprzez zmianę wartości w `GameSettings` mogło by nie działać jak powinno.

Jeżeli chodzi klasy typu `ScriptableObject` zazwyczaj używam dyrektyw preprocesora `#if UNITY_EDITOR` do liczenia/tworzenia i cache'owania zmiennych (jak np. te sumy prawdopodobieństw) w `OnValidate()`. Czasami używam `OnAfterDeserialize()` czy jakiegoś zewnętrznego managera (jeżeli logika cache'owania jest bardziej skomplikowana i bierze pod uwagę inne assety).

Poza `ScriptableObject` używam dyrektyw preprocesora lub interfejsu, dwóch klas (jedna runtimeowa, druga bardziej rozszerzona do pracy w edytorze) i DI. Dzięki temu  zamiast jednej klasy pełnej wielu dyrektyw, z pomieszaną logiką runtimeową i edytorską mam dwie klasy w której wszystko jest przejrzyste a dyrektywa jest obecna tylko i wyłącznie w Installerze gdzie binduję interfejs (przy użyciu Zenject lub Extenject).
### Alokacje pamięci
Kiedy zaczynałem miałem zamiar stworzyć projekt a później dodać paczkę Addressables i instancjonować oraz niszczyć wszelkie obiekty asynchronicznie. To automatycznie zwalniałoby pamięć zaklepywaną przez wszystko co dotyczy instancjonowanych w taki sposób obiektów.

Ostatecznie nie zrobiłem tego. Musiałbym poświęcić na to więcej czasu i nie podejrzewam, że w tym przypadku zrobiłoby to wielką różnicę. W tym projekcie gracz może jedynie grać raz za razem lub wyjść z gry. Zwalnianie pamięci nie miałoby większego sensu. Postanowiłem nie niszczyć obiektów które już są w poolu tak by gra mogła je wykorzystać przy kolejnych rozgrywkach w trakcie jednej sesji.

Kiedy użyłbym Addressables? W przypadku gdyby gra miała poziomy a poziomy różniłyby się od siebie np. tłem, rodzajami kul czy rodzajami wrogów (oczywiście gdyby te różnice byłyby na tyle duże by nie można było mieć jednego poola i zmieniać kilka wartości przy pobieraniu - jak np. zrobiłem to przy wrogach).

Ostatecznie, wykorzystanie Addressabli do wczytywania samych tekstur, muzyki, sfxów, czy particli miałoby sens.
