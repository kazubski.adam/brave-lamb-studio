﻿using Else;
using UnityEngine;

namespace UI
{
    public class GameScreen : MonoBehaviour
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private ValueView _currentScore;
        [SerializeField] private ValueView _timeLeft;
        [SerializeField] private ValueView _currentHp;
    
        private bool _isVisible;

        private void Awake () {
            Hide();
            _isVisible = _canvas.enabled;
            Score.Current.onValueChanged += UpdateCurrentScore;
        }

        public void Show () => SwitchVisibility(true);
        public void Hide () => SwitchVisibility(false);

        public void SetOnHealthChangedCallback (ValueHandler<int> currentHp) {
            UpdateCurrentHp(currentHp.Value);
            currentHp.onValueChanged += UpdateCurrentHp;
        }
    
        public void SetOnTimeLeftChangedCallback (ValueHandler<int> timeLeft) {
            UpdateTimeLeft(timeLeft.Value);
            timeLeft.onValueChanged += UpdateTimeLeft;
        }

        private void UpdateCurrentScore (int newValue) => _currentScore.SetValue(newValue);
        private void UpdateTimeLeft (int newValue) => _timeLeft.SetValue(newValue);
        private void UpdateCurrentHp (int newValue) => _currentHp.SetValue(newValue);

        private void SwitchVisibility (bool switchTo) {
            if (_isVisible == switchTo) return;
            _isVisible = switchTo;
            _canvas.enabled = switchTo;
        }
    }
}