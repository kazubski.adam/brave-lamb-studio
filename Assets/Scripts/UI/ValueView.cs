using TMPro;
using UnityEngine;

namespace UI
{
    [DefaultExecutionOrder(-1)]
    public class ValueView : MonoBehaviour
    {
        [SerializeField] private GameObject _root;
        [SerializeField] private TMP_Text _value;

        private bool _isVisible;

        private void Awake () {
            _isVisible = _root.activeSelf;
        }

        public void SetValue (int newScore) {
            _value.text = newScore.ToString();
            SwitchVisibility(true);
        }

        public void HideValue () {
            SwitchVisibility(false);
        }

        private void SwitchVisibility (bool switchTo) {
            if (_isVisible == switchTo) return;
            _isVisible = switchTo;
            _root.SetActive(switchTo);
        }
    }
}