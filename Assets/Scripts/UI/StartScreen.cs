using Else;
using UnityEngine;

namespace UI
{
    [DefaultExecutionOrder(-1)]
    public class StartScreen : MonoBehaviour
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private ValueView _bestScore;
        [SerializeField] private ValueView _mostRecentScore;

        private bool _isVisible;

        private void Awake () {
            Hide();
            _isVisible = _canvas.enabled;
        }

        public void Show () {
            _bestScore.SetValue(Score.Best);
            if(Score.MostRecent > 0) _mostRecentScore.SetValue(Score.MostRecent);
            else _mostRecentScore.HideValue();
            SwitchVisibility(true);
        }

        public void Hide () {
            SwitchVisibility(false);
        }
    
        private void SwitchVisibility (bool switchTo) {
            if (_isVisible == switchTo) return;
            _isVisible = switchTo;
            _canvas.enabled = switchTo;
        }
    }
}