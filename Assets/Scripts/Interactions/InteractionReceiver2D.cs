﻿using UnityEngine;

namespace Interactions
{
    public class InteractionReceiver2D : MonoBehaviour, IInteractionReceiver2D
    {
        [SerializeField] private InteractionHandler2D _handler;

        public bool IsEnabled => _handler.IsEnabled;
    
        public void OnTriggerEnter2D (Collider2D other) => _handler.OnTriggerEnter2D(other);

        public void DoOnTriggerEntered2D (IInteractionReceiver2D other) =>
            _handler.DoOnTriggerEntered2D(other);
    }
}