﻿namespace Interactions
{
    public interface IInteractionReceiver2D
    {
        public bool IsEnabled { get; }
        public void DoOnTriggerEntered2D (IInteractionReceiver2D other);
    }
}