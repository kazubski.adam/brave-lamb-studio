﻿using System;
using UnityEngine;

namespace Interactions
{
    [Serializable]
    public class InteractionHandler2D : MonoBehaviour, IInteractionReceiver2D
    {
        [SerializeField] private bool _triggerOnce;

        public bool IsEnabled { get; private set; }

        public void SwitchTo (bool switchTo) => IsEnabled = switchTo;

        private Action<IInteractionReceiver2D> _onTriggerEntered;

        public void RegisterTriggerEnterCallback (Action<IInteractionReceiver2D> newOnTriggerEntered) =>
            _onTriggerEntered = newOnTriggerEntered;

        public void UnRegisterTriggerEnterCallback (Action<IInteractionReceiver2D> newOnTriggerEntered) =>
            _onTriggerEntered = newOnTriggerEntered;

        // Filtering triggers between objects to make sure there will be
        // only one pair detecting each other (_triggerOnce == true) if that what is needed 
        public void OnTriggerEnter2D (Collider2D other) {
            if (!IsEnabled) return;
            var otherReceiver = other.gameObject.GetComponent<IInteractionReceiver2D>();
            if (otherReceiver is not { IsEnabled: true }) return;
        
            DoOnTriggerEntered2D(otherReceiver);
            otherReceiver.DoOnTriggerEntered2D(this);
        }

        public void DoOnTriggerEntered2D (IInteractionReceiver2D other) {
            if (_triggerOnce) SwitchTo(false);
            _onTriggerEntered?.Invoke(other);
        }
    }
}