﻿using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Ranged_Arrays
{
    [Serializable]
    public class RangedArray<TType>
    {
        [SerializeField] private RangedArrayItem<TType>[] _items;

        public int Size => _items.Length;

        public TType Random
        {
            get {
                var sum = 0;

                for (var i = 0; i < _items.Length; i++) sum += _items[i].Range;

                var randomNumber = UnityEngine.Random.Range(0, sum);
                var toCheck = 0;

                for (var i = 0; i < _items.Length; i++) {
                    toCheck += _items[i].Range;
                    if (randomNumber < toCheck) return _items[i].Value;
                }

                return _items[0].Value;
            }
        }

        public TType Last => _items[^1].Value;
    }
}