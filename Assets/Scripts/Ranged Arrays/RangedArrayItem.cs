﻿using System;
using UnityEngine;

namespace Ranged_Arrays
{
    [Serializable]
    public class RangedArrayItem<TType>
    {
        [field: SerializeField] public TType Value { get; private set; }
        [field: SerializeField, Range(0, 100)] public int Range { get; private set; }
    }
}