﻿using UnityEngine;

namespace Else
{
    public static class Score
    {
        private const string _BEST_SCORE_KEY = "bestScore";

        public static int Best
        {
            get => PlayerPrefs.GetInt(_BEST_SCORE_KEY);
            set => PlayerPrefs.SetInt(_BEST_SCORE_KEY, value);
        }

        public static ValueHandler<int> Current { get; } = new(-1);

        public static int MostRecent { get; set; } = -1;

        public static void UpdateMostRecent () {
            MostRecent = Current.Value;
        }
    
        public static void UpdateBest () {
            if (Current.Value <= Best) return;
        
            Best = Current.Value;
        }
    }
}