﻿using System;
using System.Collections.Generic;

namespace Else
{
    public class Pool<TItem>
        where TItem : class
    {
        private readonly Func<TItem> _create;
        private readonly Action<TItem> _onGet;
        private readonly Action<TItem> _onRelease;

        private readonly bool _hasGet;
        private readonly bool _hasRelease;

        private readonly List<TItem> _items = new();
        private readonly List<bool> _isAvailable = new();

        private int _searchFrom;
        private int _collectionSize;

        public Pool (Func<TItem> create, Action<TItem> onGet = null, 
                     Action<TItem> onRelease = null) {
            _create = create;
            _hasGet = onGet != null;
            _onGet = onGet;
            _hasRelease = onRelease != null;
            _onRelease = onRelease;
        }

        public TItem Get () {
            var availableIndex = _collectionSize;

            for (var i = _searchFrom; i < _collectionSize; i++) {
                if (!_isAvailable[i]) continue;
                availableIndex = i;
                break;
            }

            _searchFrom = availableIndex;

            if (availableIndex == _collectionSize) {
                _items.Add(Create());
                _isAvailable.Add(false);
            }

            var availableItem = _items[availableIndex];
            if (_hasGet) _onGet(availableItem);
            _isAvailable[availableIndex] = false;

            return availableItem;
        }

        private TItem Create () {
            var newItem = _create();
            _collectionSize++;
            return newItem;
        }

        public void Release (TItem item) {
            if (!_hasRelease) return;

            var currentIndex = CurrentIndex();
            if (currentIndex < _searchFrom) _searchFrom = currentIndex;
        
            _searchFrom = currentIndex;
            _isAvailable[currentIndex] = true;
            _onRelease(item);

            int CurrentIndex () {
                for (var i = 0; i < _items.Count; i++) {
                    if (_items[i] != item) continue;
                    return i;
                }

                return 0;
            }
        }

        public void ReleaseAll () {
            if (!_hasRelease) return;

            for (var i = 0; i < _items.Count; i++) {
                if (_isAvailable[i]) continue;
                _isAvailable[i] = true;
                _onRelease(_items[i]);
            }

            _searchFrom = 0;
        }
    }
}