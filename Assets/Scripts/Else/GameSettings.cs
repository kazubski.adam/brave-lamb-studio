﻿using System;
using Bullets;
using Enemies;
using Ranged_Arrays;
using UnityEngine;

namespace Else
{
    [CreateAssetMenu(menuName = "Game Settings", fileName = "New Game Settings", order = 0)]
    [Serializable]
    public class GameSettings : ScriptableObject
    {
        // Enemies
        [field: Header("Enemies")]
        [field: SerializeField] public Enemy EnemyPrefab { get; private set; }
        [field: SerializeField] public float EnemySpeed { get; private set; }
        [field: SerializeField] public RangedArray<int> EnemiesPerSpawnStep { get; private set; }
        [field: SerializeField] public float EnemiesSpawnStepInSeconds { get; private set; }
        [field: SerializeField] public float EnemiesOffScreenOffset { get; private set; }
        [field: SerializeField] public float DistanceBetweenLines { get; private set; }
        [field: SerializeField] public Texture[] EnemiesTextures { get; private set; }
    
        public int LinesCount => EnemiesPerSpawnStep.Last;
    
        // Player
        [field: Header("Player")]
        [field: SerializeField] public Player PlayerPrefab { get; private set; }
        [field: SerializeField] public float PlayerTargetPositionFromEdgeOffset { get; private set; }
        [field: SerializeField] public float PlayerMovementSpeed { get; private set; }
        [field: SerializeField] public float PlayerFollowUpMultiplier { get; private set; }
        [field: SerializeField] public float MaxHeight { get; private set; }
        [field: SerializeField] public int MaxHealthPoints { get; private set; }
        [field: SerializeField] public Texture PlayerTexture { get; private set; }

        // Bullets
        [field: Header("Bullets")]
        [field: SerializeField] public Bullet BulletPrefab { get; private set; }
        [field: SerializeField] public float BulletsSpawnStepInSeconds { get; private set; }
        [field: SerializeField] public float BulletsSpeed { get; private set; }

        // ELse
        [field: Header("Else")]
        [field: SerializeField] public int LevelDurationInSeconds { get; private set; }
        [field: SerializeField] public Material PlaneMaterialReference { get; private set; }
    }
}