﻿using System;

namespace Else
{
    public class ValueHandler<TType>
    {
        private TType _value;

        public ValueHandler (TType newValue) => _value = newValue;
    
        public TType Value
        {
            get => _value;
            set {
                _value = value;
                onValueChanged?.Invoke(_value);
            }
        }
    
        public Action<TType> onValueChanged;

        public void Refresh () => onValueChanged?.Invoke(_value);
    }
}