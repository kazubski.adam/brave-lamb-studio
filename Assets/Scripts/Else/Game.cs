﻿using States;
using UnityEngine;

namespace Else
{
    public class Game : MonoBehaviour
    {
        private static readonly int _animate = Shader.PropertyToID("_Animate");
    
        [SerializeField] private StateMachine _stateMachine;
        [SerializeField] private GameSettings _gameSettings;
        [SerializeField] private GameObject _background;

        private static Game _instance;
        public static GameSettings Settings => _instance._gameSettings;
    
        private void Awake () {
            _instance = this;
            _stateMachine.Initialize();
            _stateMachine.MainMenu.SwitchTo();
            _background.SetActive(true);
        }

        private void Update () => _stateMachine.CurrentState.OnUpdate();
    }
}