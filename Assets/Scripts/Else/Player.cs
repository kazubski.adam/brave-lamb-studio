﻿using System;
using Bullets;
using Interactions;
using UnityEngine;

namespace Else
{
    [Serializable]
    public class Player : MonoBehaviour
    {
        // 0 - right
        // 1 - straight
        // 2 - left
        private static readonly int _mode = Shader.PropertyToID("_Mode");
        private static readonly int _mainTex = Shader.PropertyToID("_MainTex");

        [SerializeField] private Rigidbody2D _rigidbody;
        [SerializeField] private Transform _bulletsSpawnPoint;
        [SerializeField] private MeshRenderer _meshRenderer;
        [SerializeField] private InteractionHandler2D _interactionHandler2D;
        
        private BulletsSpawner _bulletsSpawner;
        private Vector2 _targetPosition;
        private Material _material;
        private int _previousMovementDirection;

        public ValueHandler<int> Health { get; private set; }

        public void Awake () {
            _interactionHandler2D.SwitchTo(true);
            _interactionHandler2D.RegisterTriggerEnterCallback(OnHitTarget);
            _bulletsSpawner = new BulletsSpawner(Game.Settings.BulletPrefab, _bulletsSpawnPoint);
            var leftEdgePosition = Camera.main.ScreenToWorldPoint(new Vector3(0, 0)).x;
            var newTargetWidth = leftEdgePosition 
                               + Game.Settings.PlayerTargetPositionFromEdgeOffset;
        
            _targetPosition = new Vector2(newTargetWidth, 0);
            transform.position = _targetPosition;

            _material = new Material(Game.Settings.PlaneMaterialReference);
            _material.SetInt(_mode, 1);
            _material.SetTexture(_mainTex, Game.Settings.PlayerTexture);
            _meshRenderer.sharedMaterial = _material;

            Health = new ValueHandler<int>(Game.Settings.MaxHealthPoints);
        }

        private void OnHitTarget (IInteractionReceiver2D other) => Health.Value--;

        private void OnDestroy () => _bulletsSpawner.ClearAll();

        private void Update () {
            MoveTowardsTargetPosition();
            ManageFiring();
        }

        private void FixedUpdate () {
            UpdateTargetPosition();
        }

        private void UpdateTargetPosition () {
            var movementDirection = 0;
            if (Input.GetKey(KeyCode.UpArrow)) movementDirection++;
            if (Input.GetKey(KeyCode.DownArrow)) movementDirection--;

            if (movementDirection != _previousMovementDirection) {
                _previousMovementDirection = movementDirection;
                var normalizedMovement = movementDirection + 1;
            
                _material.SetInt(_mode, normalizedMovement);
            }
        
            if (movementDirection == 0) return;

            var movementSpeed = Game.Settings.PlayerMovementSpeed * Time.fixedDeltaTime;
            var step = movementDirection * movementSpeed;
            var rawTargetHeight = _targetPosition.y + step;
            var max = Game.Settings.MaxHeight;
            var newTargetHeight = Mathf.Clamp(rawTargetHeight, -max, max);
            var leftEdgePosition = Camera.main.ScreenToWorldPoint(new Vector3(0, 0)).x;
            var newTargetWidth = leftEdgePosition 
                               + Game.Settings.PlayerTargetPositionFromEdgeOffset;
        
            _targetPosition = new Vector2(newTargetWidth, newTargetHeight);
        }

        private void MoveTowardsTargetPosition () {
            var currentPosition = _rigidbody.position;
            var distance = Vector2.Distance(currentPosition, _targetPosition);

            // Stop Following
            if (distance < 0.01f) {
                _rigidbody.MovePosition(_targetPosition);
                _rigidbody.velocity = Vector2.zero;
                return;
            }

            // Follow
            var targetVelocity = (_targetPosition - currentPosition) / Time.fixedDeltaTime;
            _rigidbody.velocity = targetVelocity * Game.Settings.PlayerFollowUpMultiplier;
        }
    
        private void ManageFiring () {
            _bulletsSpawner.Switch(Input.GetKey(KeyCode.Space));
            _bulletsSpawner.OnUpdate();
        }
    }
}