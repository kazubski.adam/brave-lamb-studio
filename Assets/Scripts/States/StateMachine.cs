﻿using System;
using UnityEngine;

namespace States
{
    [Serializable]
    public class StateMachine
    {
        [field: SerializeField] public MainMenu MainMenu { get; private set; }
        [field: SerializeField] public InGame InGame { get; private set; }

        public State CurrentState { get; private set; } = new PlaceholderState();

        public void SwitchTo (State newState) {
            CurrentState.OnExit();
            CurrentState = newState;
            CurrentState.OnEnter();
        }

        public void Initialize () {
            MainMenu.Initialize(this);
            InGame.Initialize(this);
        }
    }
}