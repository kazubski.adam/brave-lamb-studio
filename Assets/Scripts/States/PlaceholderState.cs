﻿namespace States
{
    public class PlaceholderState : State
    {
        public override void OnInitialize () { }
        public override void OnEnter () { }
        public override void OnExit () { }
        public override void OnUpdate () { }
    }
}