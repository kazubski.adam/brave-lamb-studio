﻿using System;
using Else;
using Enemies;
using UI;
using UnityEngine;
using Object = UnityEngine.Object;

namespace States
{
    [Serializable]
    public class InGame : State
    {
        [SerializeField] private GameScreen _gameScreen;

        private readonly EnemySpawner _enemySpawner = new();
        private readonly ValueHandler<int> _timeLeft = new(0);
        private Player _player;
        private float _currentTime;

        public override void OnInitialize () => _enemySpawner.Initialize();

        public override void OnEnter () {
            Score.Current.Value = 0;
            _player = Object.Instantiate(Game.Settings.PlayerPrefab);
            _player.Health.onValueChanged += OnHealthChanged;
            _enemySpawner.StartSpawning();
            _timeLeft.Value = Game.Settings.LevelDurationInSeconds;
            _currentTime = 0;
        
            _gameScreen.SetOnHealthChangedCallback(_player.Health);
            _gameScreen.SetOnTimeLeftChangedCallback(_timeLeft);
            _gameScreen.Show();
        }

        public override void OnExit () {
            Object.Destroy(_player.gameObject);
            _gameScreen.Hide();
            _enemySpawner.ClearAll();
        }

        public override void OnUpdate () {
            _currentTime += Time.deltaTime;

            if (_currentTime > 1) {
                _timeLeft.Value--;
                _currentTime--;
            }
        
            if (_timeLeft.Value <= -1) {
                LevelCompleted();
                return;
            }

            _enemySpawner.OnUpdate();
        }

        private void OnHealthChanged (int newValue) {
            if (newValue > 0) return;

            GameOver();
        }

        private void LevelCompleted () {
            Score.UpdateMostRecent();
            Score.UpdateBest();
            machine.MainMenu.SwitchTo();
        }

        private void GameOver () {
            Score.Current.Value = 0;
            machine.MainMenu.SwitchTo();
        }
    }
}