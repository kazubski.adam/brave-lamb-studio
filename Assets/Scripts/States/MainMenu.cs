﻿using System;
using UI;
using UnityEngine;

namespace States
{
    [Serializable]
    public class MainMenu : State
    {
        [SerializeField] private StartScreen _startScreen;

        private bool _wasUp;

        public override void OnInitialize () { }

        public override void OnEnter () {
            _startScreen.Show();
        }

        public override void OnExit () {
            _startScreen.Hide();
        }

        public override void OnUpdate () {
            if (!Input.anyKey) {
                _wasUp = true;
                return;
            }

            if (!_wasUp) return;
        
            _wasUp = false;
            machine.InGame.SwitchTo();
        }
    }
}