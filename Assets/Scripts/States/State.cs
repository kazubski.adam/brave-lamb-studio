﻿namespace States
{
    public abstract class State
    {
        protected StateMachine machine;

        public void Initialize (StateMachine newMachine) {
            machine = newMachine;
            OnInitialize();
        }

        public void SwitchTo () => machine.SwitchTo(this);
    
        public abstract void OnEnter ();
        public abstract void OnExit ();
        public abstract void OnUpdate ();

        public abstract void OnInitialize ();
    }
}