﻿using System.Collections.Generic;
using UnityEngine;

namespace Enemies
{
    public class EnemyMaterials
    {

        private readonly EnemyMaterialType[] _materialTypes;

        public EnemyMaterials (Material reference, IReadOnlyList<Texture> textures, int materialsPerType) {
            _materialTypes = new EnemyMaterialType[textures.Count];
        
            for (var i = 0; i < textures.Count; i++) {
                _materialTypes[i] = new EnemyMaterialType(reference, textures[i], materialsPerType, 1);
            }
        }

        public Material GetRandom () => _materialTypes[Random.Range(0, _materialTypes.Length)].GetRandom();
       
        private class EnemyMaterialType
        {
            private static readonly int _timeOffset = Shader.PropertyToID("_TimeOffset");
            private static readonly int _mainTex = Shader.PropertyToID("_MainTex");
            private readonly Material[] _instances;

            public EnemyMaterialType (Material reference, Texture texture, 
                                      int materialsPerType, float timeOffsetStep) {
                _instances = new Material[materialsPerType];

                for (var i = 0; i < materialsPerType; i++) {
                    _instances[i] = new Material(reference);
                    _instances[i].SetFloat(_timeOffset, i * timeOffsetStep);
                    _instances[i].SetTexture(_mainTex, texture);
                }
            }

            public Material GetRandom () => _instances[Random.Range(0, _instances.Length)];
        }
    }
}