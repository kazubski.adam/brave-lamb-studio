﻿using Else;
using Interactions;
using UnityEngine;

namespace Enemies
{
    public class Enemy : MonoBehaviour
    {
        public static Pool<Enemy> pool;

        [SerializeField] private InteractionHandler2D _interactionHandler2D;
    
        [field: SerializeField] public GameObject Root { get; private set; }
        [field: SerializeField] public Rigidbody2D Rigidbody { get; private set; }
        [field: SerializeField] public MeshRenderer MeshRenderer { get; private set; }

        private void Awake () => _interactionHandler2D.RegisterTriggerEnterCallback(OnHitTarget);
    
        private void OnHitTarget (IInteractionReceiver2D other) => Release();

        private void Update () {
            var currentPosition = Root.transform.position.x;
            var leftEdgePosition = Camera.main.ScreenToWorldPoint(new Vector3(0, 0)).x;
            var leftEdgePositionWithOffset = leftEdgePosition 
                                           - Game.Settings.EnemiesOffScreenOffset;

            if (currentPosition > leftEdgePositionWithOffset) return;

            Release();
        }

        private void Release () => pool.Release(this);

        public void Switch (bool switchTo) {
            Root.SetActive(switchTo);
            _interactionHandler2D.SwitchTo(switchTo);
        }
    }
}