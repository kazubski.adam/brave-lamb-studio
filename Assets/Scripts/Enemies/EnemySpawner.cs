﻿using Else;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemies
{
    public class EnemySpawner
    {
        private readonly EnemyPool _enemies = new();
        private EnemyMaterials _enemyMaterials;
        private float _currentTime = -1f;
    
        public void Initialize () {
            var reference = Game.Settings.PlaneMaterialReference;
            var textures = Game.Settings.EnemiesTextures;
            _enemyMaterials = new EnemyMaterials(reference, textures, 2);
        }
    
        public void StartSpawning () => _currentTime = 0;
        public void ClearAll () => _enemies.ReleaseAll();

        public void OnUpdate () {
            if (_currentTime < 0) return;

            _currentTime += Time.deltaTime;
            if (_currentTime < Game.Settings.EnemiesSpawnStepInSeconds) return;

            _currentTime = 0;

            // Spawn Enemies
            var indexes = ShuffledLinesIndexes();
            var heights = LinesHeights();
            var enemiesToSpawn = Game.Settings.EnemiesPerSpawnStep.Random;
            var rightEdgePosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0)).x;
            var rightEdgePositionWithOffset = rightEdgePosition
                                            + Game.Settings.EnemiesOffScreenOffset;

            for (var i = 0; i < enemiesToSpawn; i++) {
                var newPosition = new Vector3(rightEdgePositionWithOffset, heights[indexes[i]], 0);
                var newEnemy = _enemies.Get();

                newEnemy.Switch(true);
                newEnemy.Root.gameObject.transform.position = newPosition;
                newEnemy.Rigidbody.velocity = Vector2.left * Game.Settings.EnemySpeed;
                newEnemy.MeshRenderer.sharedMaterial = _enemyMaterials.GetRandom();
            }
        }

        private static int[] ShuffledLinesIndexes () {
            var indexesToShuffle = Game.Settings.LinesCount;
            var newIndexes = new int[indexesToShuffle];

            for (var i = 0; i < indexesToShuffle; i++) newIndexes[i] = i;

            while (indexesToShuffle > 1) {  
                indexesToShuffle--;
                var currentIndex = indexesToShuffle;
                var nextIndex = Random.Range(0, indexesToShuffle + 1);
                (newIndexes[nextIndex], newIndexes[currentIndex]) =
                    (newIndexes[currentIndex], newIndexes[nextIndex]);
            } 

            return newIndexes;
        }

        private static float[] LinesHeights () {
            var linesCount = Game.Settings.LinesCount;
            var distanceBetweenLines = Game.Settings.DistanceBetweenLines;
            var newHeights = new float[linesCount];
            var firstHeight = (linesCount - 1) / 2f * distanceBetweenLines;

            for (var i = 0; i < linesCount; i++) newHeights[i] = firstHeight - i * distanceBetweenLines;
        
            return newHeights;
        }
    }
}