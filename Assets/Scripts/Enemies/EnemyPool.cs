﻿using Else;
using Object = UnityEngine.Object;

namespace Enemies
{
    public class EnemyPool
    {
        private readonly Pool<Enemy> _pool;

        public EnemyPool () {
            _pool = new Pool<Enemy>(Create, onRelease: Release);

            Enemy.pool = _pool;
        }

        public void ReleaseAll () => _pool.ReleaseAll();
        public Enemy Get () => _pool.Get();
        private static Enemy Create () => Object.Instantiate(Game.Settings.EnemyPrefab);
        private static void Release (Enemy enemy) => enemy.Switch(false);
    }
}