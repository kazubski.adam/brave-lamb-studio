﻿using Else;
using UnityEngine;

namespace Bullets
{
    public class BulletsSpawner
    {
        private readonly BulletPool _bullets;
        private readonly Transform _pivot;
        private float _currentTime;
        private bool _isShooting;

        public BulletsSpawner (Bullet bulletPrefab, Transform newPivot) {
            _bullets = new BulletPool(bulletPrefab);
            _pivot = newPivot;
            _currentTime = Game.Settings.BulletsSpawnStepInSeconds;
        }

        public void Switch (bool switchTo) => _isShooting = switchTo;
        public void ClearAll () => _bullets.ReleaseAll();

        public void OnUpdate () {
            if (_currentTime < 0) return;

            _currentTime += Time.deltaTime;
            if (_currentTime < Game.Settings.BulletsSpawnStepInSeconds) return;
            if (!_isShooting) return;

            _currentTime = 0;

            var newBullet = _bullets.Get();

            newBullet.Switch(true);
            newBullet.transform.position = _pivot.position;
            newBullet.Rigidbody.velocity = Vector2.right * Game.Settings.BulletsSpeed;
        }
    }
}