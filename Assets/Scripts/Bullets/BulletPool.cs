﻿using Else;
using Object = UnityEngine.Object;

namespace Bullets
{
    public class BulletPool
    {
        private readonly Bullet _bulletPrefab;
        private readonly Pool<Bullet> _pool;

        public BulletPool (Bullet newBulletPrefab) {
            _bulletPrefab = newBulletPrefab;
            _pool = new Pool<Bullet>(Create, onRelease: Release);

            Bullet.objectPool = _pool;
        }

        public void ReleaseAll () => _pool.ReleaseAll();
        public Bullet Get () => _pool.Get();
        private Bullet Create () => Object.Instantiate(_bulletPrefab);
        private static void Release (Bullet bullet) => bullet.Switch(false);
    }
}