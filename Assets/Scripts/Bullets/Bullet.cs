﻿using Else;
using Interactions;
using UnityEngine;

namespace Bullets
{
    public class Bullet : MonoBehaviour
    {
        public static Pool<Bullet> objectPool;

        [SerializeField] private InteractionHandler2D _interactionHandler2D;

        [field: SerializeField] public GameObject Root { get; private set; }
        [field: SerializeField] public Rigidbody2D Rigidbody { get; private set; }

        private void Awake () => _interactionHandler2D.RegisterTriggerEnterCallback(OnHitTarget);

        private void OnHitTarget (IInteractionReceiver2D other) {
            Score.Current.Value++;
            Release();
        }

        private void Update () {
            var currentPosition = Root.transform.position.x;
            var rightEdgePosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0)).x;

            if (currentPosition < rightEdgePosition) return;
        
            if (_interactionHandler2D.IsEnabled) _interactionHandler2D.SwitchTo(false);

            var rightEdgePositionWithOffset = rightEdgePosition + Game.Settings.EnemiesOffScreenOffset;

            if (currentPosition < rightEdgePositionWithOffset) return;

            Release();
        }

        private void Release () => objectPool.Release(this);

        public void Switch (bool switchTo) {
            Root.SetActive(switchTo);
            _interactionHandler2D.SwitchTo(switchTo);
        }
    }
}